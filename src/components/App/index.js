import React, { useState } from 'react';
import './style.css'
const initTasks=[
  {id:1, title:'Open your eyes', done:true},
  {id:2,title:'Chek you smartphone',done:false},
  {id:3, title:'Brash your teeth', done:false}
]

const App=() =>{
  const[tasks,SetTask]=useState(initTasks)
const [newTask,setNewTask]=useState('')
const changeTaskStatus=(element)=>{
  SetTask(tasks.map(item=>item.id===element.id?{...item,done:!item.done}:item))
}

const enterNewTask=(e)=>setNewTask(e.target.value)
const addNewTask=()=>{
  SetTask(
    [...tasks,
      {id:tasks.length+1,title:newTask,done:false}
    ]
  )
  setNewTask('')
}

  return (
    <div className='container'>
    <div className='box'>
      <input type='text' value={newTask} onChange={enterNewTask}/>
      <button onClick={addNewTask}>Add new task</button>
    </div>
<div className='box'>
<ul className="task-list">
  {
    tasks.map(el=><li key={el.id} onClick={()=>changeTaskStatus(el)} className={el.done?'task-done':''}>{el.title}</li>)
  }
</ul>
</div>
    </div>
  )}


export default App;
